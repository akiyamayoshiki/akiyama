package jp.alhinc.akiyama_yoshiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Sales {
	public static void main(String[] args) {

		try {
			if (args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			String path0 = args[0];
			HashMap<String, String> branch = new HashMap<String, String>();
			HashMap<String, Long> syukei = new HashMap<String, Long>();// 集計マップ

			// 支店定義ファイルのエラーチェック&リスト読み込み処理
			if (readlst(path0, branch, syukei) == false) {
				return;
			}

			// ここからしゅうけい
			BufferedReader br = null;
			String path = args[0];
			File dir = new File(path);
			File[] files = dir.listFiles();
			// エラー処理開始
			ArrayList<String> serialList = new ArrayList<String>();
			ArrayList<Integer> serialList2 = new ArrayList<Integer>();

			String[] listnumber = null;
			for (int i = 0; i < files.length; i++) {

				if (files[i].getName().matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
					serialList.add(files[i].getName());

				}
			}

			for (int i = 0; i < serialList.size(); i++) {

				listnumber = serialList.get(i).split(".rcd", 0);
				int numbercheck = Integer.valueOf(listnumber[0]).intValue();
				serialList2.add(numbercheck);
				for (int n = 0; n < serialList2.size() - 1; n++) {

					if (serialList2.get(n + 1) - serialList2.get(n) != 1) {

						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
				}
			}

			// ここから集計するファイルの中身を取得

			for (int i = 0; i < serialList.size(); i++) {
				try {
					File rcd = new File(args[0], serialList.get(i));
					FileReader fr = new FileReader(rcd);
					br = new BufferedReader(fr);
					String line;
					// String[] contents = null;

					ArrayList<String> syukeiList = new ArrayList<String>();
					while ((line = br.readLine()) != null) {
						syukeiList.add(line);

						// エラー処理 2-3 syukeiList.get(0)がリストの支店コード
						if (branch.get(syukeiList.get(0)) == null) {
							System.out.println(rcd.getName() + "の支店コードが不正です");
							return;
						}
					}

					// エラー処理 2-4

					if (syukeiList.size() != 2) {
						System.out.println(rcd.getName() + "のフォーマットが不正です");
						return;
					}

					// System.out.println(syukeiList.get(1));
					long sum = Long.parseLong(syukeiList.get(1));
					long sum2 = syukei.get(syukeiList.get(0));
					long total = sum + sum2;
					// System.out.println(total);

					// エラー処理
					long last = 9999999999L;
					if (total > last) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					syukei.put(syukeiList.get(0), total);
					// System.out.println(syukei.get(aa+ln));
				} catch (FileNotFoundException e) {

					// 指定したファイルが存在しなかった時のトライキャッチ
					System.out.println("予期せぬエラーが発生しました");
					return;

				} catch (IOException e) {
					// 指定したファイルの入出力ができなかった時のトライキャッチ
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							// 指定したファイルの入出力ができなかった時のトライキャッチ
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}

				// System.out.println(syukei.values());

			} // ここでfor文終わってる

			if (output(path0, branch, syukei) == false) {
				return;
			}

		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

	}// メインメソッドここで終了

	// リスト読み込みメソッド
	static boolean readlst(String path0, HashMap<String, String> branch, HashMap<String, Long> syukei) {

		// lstファイルが存在するかどうかを確認
		File file5 = new File(path0, "branch.lst");
		if (!file5.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}

		BufferedReader br = null;
		try {
			File file = new File(path0, "branch.lst");
			br = new BufferedReader(new FileReader(file));
			String brch;
			String[] contents = null;

			while ((brch = br.readLine()) != null) {
				contents = brch.split(",", 0);

				if (!contents[0].matches("[0-9]{3}") || contents.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					br.close();
					return false;
				}

				branch.put(contents[0], contents[1]);
				syukei.put(contents[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} catch (ArrayIndexOutOfBoundsException e) {
			// 配列の数に関するトライキャッチ
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// ファイルリーダーが閉じられていない時のトライキャッチ
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	// outファイル出力メソッド開始
	static boolean output(String path0, HashMap<String, String> branch, HashMap<String, Long> syukei) {

		BufferedWriter bw = null;
		try {
			// 出力先を作成する
			File file = new File(path0, "branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			// 内容を書き込む

			for (String s : branch.keySet()) {
				bw.write(s + "," + branch.get(s) + "," + syukei.get(s) + "\r\n");
			}

		} catch (IOException ex) {
			// 例外時処理
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					// ファイルリーダーが閉じられていない時のトライキャッチ
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}